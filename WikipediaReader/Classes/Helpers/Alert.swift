import UIKit

class Alert {
    static func show(title: String?, message: String?, inController: UIViewController) {
        let okAction = UIAlertAction(title: "Ok", style: .Cancel, handler: nil)
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .Alert)
        alertController.addAction(okAction)
        inController.presentViewController(alertController, animated: true, completion: nil)
    }
}
