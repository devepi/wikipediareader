import UIKit

protocol DataSource : UITableViewDataSource {
    var delegate: DataSourceDelegate? { get set }
    var items: [String : String]! { get }

    func loadData()
}

protocol DataSourceDelegate : class {
    func didStartLoading(dataSource: DataSource)
    func didFinishLoading(dataSource: DataSource)
}
