import UIKit

class FavoritesDatasource : NSObject, DataSource {
    static let NSUserDefaultsItemsKey = "NSUserDefaultsItemsKey"

    static let instance = FavoritesDatasource()

    weak var delegate: DataSourceDelegate?

    var items: [String : String]! = [:]

    private override init() {
        super.init()
        loadData()
    }

    // MARK: - basic operations with items

    func add(title: String, url: NSURL) {
        self.items[title] = url.absoluteString
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0)) {
            self.saveData()
        }
        self.delegate?.didFinishLoading(self)
    }

    func removeItemWithIndexPath(indexPath: NSIndexPath) {
        var array = Array(self.items.keys)
        items.removeValueForKey(array[indexPath.row])
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0)) {
            self.saveData()
        }
    }

    // MARK: - save & restore favorites items

    func loadData() {
        self.delegate?.didStartLoading(self)

        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0)) {
            let defaults = NSUserDefaults.standardUserDefaults()
            let items = defaults.objectForKey(FavoritesDatasource.NSUserDefaultsItemsKey) as! [String:String]?
            self.items = items ?? [:]
            dispatch_async(dispatch_get_main_queue()) {
                self.delegate?.didFinishLoading(self)
            }
        }
    }

    private func saveData() {
        let defaults = NSUserDefaults.standardUserDefaults()
        defaults.setObject(items, forKey: FavoritesDatasource.NSUserDefaultsItemsKey)
        defaults.synchronize()
    }

    // MARK: - UITableViewDataSource

    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return items.count
    }

    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let key = Array(self.items.keys)[indexPath.row] as String
        let cell = tableView.dequeueReusableCellWithIdentifier("FavoriteCell", forIndexPath: indexPath)
        cell.textLabel?.text = key
        cell.detailTextLabel?.text = items[key]
        return cell
    }

    func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        return true
    }

    func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if editingStyle == .Delete {
            self.removeItemWithIndexPath(indexPath)
            tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
        }
    }
}
