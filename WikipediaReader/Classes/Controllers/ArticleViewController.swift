import UIKit
import WebKit
import SafariServices

class ArticleViewController: UIViewController, WKNavigationDelegate, FavoritesDelegate {

    var webView: WKWebView?
    var refreshControl: UIRefreshControl!

    override func viewDidLoad() {
        super.viewDidLoad()

        // webview
        self.webView = WKWebView(frame:self.view.bounds, configuration: WKWebViewConfiguration())
        self.webView?.navigationDelegate = self
        self.webView?.translatesAutoresizingMaskIntoConstraints = false
        self.view.addSubview(webView!)
        self.view.clipsToBounds = true

        // webview constraints
        let views = ["webView": webView!]
        let vertical = NSLayoutConstraint.constraintsWithVisualFormat(
                "V:|[webView]|",
                options: NSLayoutFormatOptions.AlignAllCenterY,
                metrics: nil,
                views: views
        )
        let horizontal = NSLayoutConstraint.constraintsWithVisualFormat(
                "H:|[webView]|",
                options: NSLayoutFormatOptions.AlignAllCenterX,
                metrics: nil,
                views: views
        )
        self.view.addConstraints(vertical)
        self.view.addConstraints(horizontal)

        // refresh control
        self.refreshControl = UIRefreshControl()
        self.refreshControl.addTarget(self, action: "loadRandomPage", forControlEvents: UIControlEvents.ValueChanged)
        self.webView?.scrollView.addSubview(refreshControl!)

        // initial load
        self.fixLoadingOffset()
        self.refreshControl.beginRefreshing()
        loadRandomPage()
    }

    func fixLoadingOffset() {
        let point = CGPointMake(0, self.webView!.scrollView.contentOffset.y - self.refreshControl.frame.size.height)
        self.webView?.scrollView.setContentOffset(point, animated:true)
    }

    func showPage(url: String) {
        self.reloadRequestWithUrl(url)
    }

    func reloadRequestWithUrl(url: String) {
        let url = NSURL(string: url)!
        let request = NSURLRequest(URL: url, cachePolicy: .UseProtocolCachePolicy, timeoutInterval: 10)
        self.webView?.loadRequest(request)
    }

    // MARK: - Actions

    @IBAction func loadRandomPage() {
        self.reloadRequestWithUrl("https://en.wikipedia.org/wiki/Special:Random")
    }

    @IBAction func shareArticle() {
        var items : [AnyObject] = []
        if (self.webView?.URL != nil) {
            items.append(self.webView!.URL!)
        }
        if (self.webView?.title != nil) {
            items.append(self.webView!.title!)
        }
        let activityController = UIActivityViewController(activityItems: items, applicationActivities: nil)
        self.presentViewController(activityController, animated: true, completion: nil)
    }

    @IBAction func addToFavorites() {
        if (self.webView?.URL != nil) {
            FavoritesDatasource.instance.add(self.webView!.title!, url: self.webView!.URL!)
        }
    }

    // MARK: - WKNavigationDelegate

    func webView(webView: WKWebView, didStartProvisionalNavigation navigation: WKNavigation!) {
        self.refreshControl.beginRefreshing()
    }

    func webView(webView: WKWebView, didFinishNavigation navigation: WKNavigation!) {
        self.refreshControl.endRefreshing()
    }

    func webView(webView: WKWebView, didFailProvisionalNavigation navigation: WKNavigation!, withError error: NSError) {
        if error.code != NSURLErrorCancelled {
            Alert.show("Error loading page", message: error.localizedDescription, inController: self)
        }
        self.refreshControl.endRefreshing()
    }

    func webView(webView: WKWebView, decidePolicyForNavigationAction navigationAction: WKNavigationAction, decisionHandler: (WKNavigationActionPolicy) -> Void) {
        if navigationAction.navigationType == .LinkActivated {
            let controller = SFSafariViewController(URL: navigationAction.request.URL!)
            self.presentViewController(controller, animated: true, completion: nil)
            decisionHandler(.Cancel);
            return
        } else {
            decisionHandler(.Allow);
        }
    }

    // MARK: - FavoritesViewControllerDelegate

    func didSelectUrl(url: String?) {
        if let page = url {
            self.reloadRequestWithUrl(page)
        } else {
            self.loadRandomPage()
            self.fixLoadingOffset()
        }
    }
}
