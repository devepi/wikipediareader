import UIKit

class RootViewController : UISplitViewController {

    var favoritesViewController: FavoritesViewController! {
        get {
            let navigationController = self.viewControllers.first as! UINavigationController
            let viewController = navigationController.viewControllers[0] as UIViewController
            return viewController as! FavoritesViewController
        }
    }

    var articleViewController: ArticleViewController? {
        get {
            if self.viewControllers.count > 1 {
                let navigationController = self.viewControllers[1] as! UINavigationController
                let viewController = navigationController.viewControllers[0] as UIViewController
                return viewController as? ArticleViewController
            }
            return nil
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        self.favoritesViewController.favoritesDelegate = self.articleViewController
    }

}