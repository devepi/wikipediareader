import UIKit

protocol FavoritesDelegate : class {
    func didSelectUrl(url: String?)
}

class FavoritesViewController : UITableViewController, DataSourceDelegate {

    var dataSource: FavoritesDatasource!
    weak var favoritesDelegate : FavoritesDelegate?

    override func viewDidLoad() {
        super.viewDidLoad()

        self.dataSource = FavoritesDatasource.instance
        self.dataSource.delegate = self
        self.tableView.dataSource = self.dataSource

        self.refreshControl = UIRefreshControl()
        self.refreshControl?.addTarget(self, action: "refresh", forControlEvents: UIControlEvents.ValueChanged)
    }

    func refresh() {
        dataSource.loadData()
    }

    func showSelectedPage(url: String?) {
        self.favoritesDelegate?.didSelectUrl(url)
        if let viewController = self.favoritesDelegate as? UIViewController {
            splitViewController?.showDetailViewController(viewController.navigationController!, sender: nil)
        }
    }

    // MARK: - Actions

    @IBAction func showRandomPage() {
        self.showSelectedPage(nil)
    }

    // MARK: - DataSourceDelegate

    func didStartLoading(dataSource: DataSource) {
        self.refreshControl?.beginRefreshing()
    }

    func didFinishLoading(dataSource: DataSource) {
        self.refreshControl?.endRefreshing()
        self.tableView.reloadData()
    }

    // MARK: - UITableViewDelegate

    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        tableView.deselectRowAtIndexPath(indexPath, animated: true)
        let key = Array(dataSource.items.keys)[indexPath.row]
        let url = dataSource.items[key]
        self.showSelectedPage(url)
    }

}
